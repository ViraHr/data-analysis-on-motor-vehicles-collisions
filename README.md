Data Analysis on Motor Vehicle Collisions

_ Project Objective:
Utilize functioning BI Applications in order to design and develop a public, cloud-based Data Warehouse in accordance with the Kimball Lifecycle.

_ Project Tools:
Python – data integration,
Oracle – data warehousing,
Tableau – Business Intelligence.

_ Motivation for project: 
The motivation of this project is to discover how and why car crashes occur within each borough between peak and off hours, within the timeframe of years.

_ Description of the issues or opportunities the project will address:
With close to 8.7 million people living with NYC’s five boroughs, a major concern of city life is the frequent occurrence of motor vehicle collisions. 

_ Project Business or Organization Value:
By obtaining a better understanding of why these motor vehicles accidents happen in NYC, we will be able to improve city life by making suggestions regarding how to avoid areas of high traffic at certain times of the day.

_ Data Source:
The dataset is from NYC Open Data. It contains 29 columns, 1,829,220 rows.

Link to the data source: https://data.cityofnewyork.us/Public-Safety/Motor-Vehicle-Collisions-Crashes/h9gi-nx95?fbclid=IwAR0nzadmfp25wAsBn6kAKB2JlVpFhQnjzAAu3l2fQ3Er22uQoXYQuSf0p1s

_ List of Data Warehouse Key Performance Indicators:

    1.	Top factors or the crash / Borough
    2.      Distribution of accidents / Borough
    3.	Volume of Car Accidents / Day
    4.	Injuries & Deaths / Year
    5.	Distribution of accidents / Zip Code  

_ Dimensional Model:
This project’s Dimensional Model is structured by 4 dimensional tables and 2 fact tables. 

_ ETL Process: 
The ETL process was done by using the Anaconda Jupyter Notebook and Python programming language by following these steps: 

    1. All necessary libraries were downloaded.  
    2. The dataset was extracted from the source. 
    3. Data transformation was performed through data cleansing. 
    4. The dimensional and fact tables were built using the cleaned data, which were then loaded to the data warehouse.  

_ Data Visualization:
Two dashboards were built to provide us with a larger picture of motor vehicle accidents in NYC. 

